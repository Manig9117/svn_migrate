
require (
	github.com/araddon/dateparse v0.0.0-20210207001429-0eec95c9db7e
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/disintegration/imaging v1.6.2
	github.com/djherbis/times v1.4.0
	github.com/dsoprea/go-exif/v2 v2.0.0-20200807075213-089aa48c91e6 // indirect
	github.com/dsoprea/go-exif/v3 v3.0.0-20200807075213-089aa48c91e6
	github.com/dsoprea/go-heic-exif-extractor v0.0.0-20200717090456-b3d9dcddffd1
	github.com/dsoprea/go-iptc v0.0.0-20200610044640-bc9ca208b413 // indirect
	github.com/dsoprea/go-jpeg-image-structure v0.0.0-20210128210355-86b1014917f2
	github.com/dsoprea/go-logging v0.0.0-20200710184922-b02d349568dd // indirect
	github.com/dsoprea/go-photoshop-info-format v0.0.0-20200610045659-121dd752914d
	)
